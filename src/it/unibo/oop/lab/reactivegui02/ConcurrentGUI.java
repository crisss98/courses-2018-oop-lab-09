package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/** 
 * GUI for the exercise.
 */
public class ConcurrentGUI extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final double WIDTH_PERC = 0.3;
	private static final double HEIGHT_PERC = 0.1;
	protected final JLabel label = new JLabel();
	protected final JButton up = new JButton("up");
	protected final JButton down = new JButton("down");
	protected final JButton stop = new JButton("stop");
	protected final Agent agent;
	protected final Thread th;
	/**
	 * Builds the GUI.
	 */
	public ConcurrentGUI() {
		super();
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final JPanel panel = new JPanel();
		panel.add(label);
		panel.add(up);
		panel.add(down);
		panel.add(stop);
		this.setContentPane(panel);
		this.setVisible(true);
		/**
		 * this is the agent for multi-threading purposes.
		 */
		this.agent = new Agent();
		th = new Thread(agent);
		
		/**
		 * listener for the stop button.
		 */
		stop.addActionListener(new ActionListener() {
			/**
			 * @param e
			 * 			Action that will be performed by the event.
			 */
			public void actionPerformed(final ActionEvent e) {
				agent.stopCounting();
				up.setEnabled(false);
				down.setEnabled(false);
				stop.setEnabled(false);
			}
		});
		
		/**
		 * listener for up button.
		 */
		up.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!th.isAlive()) {
					th.start();
				}
				agent.upOrDown = true;
			}
		});
		
		/**
		 * listener for down button.
		 */
		down.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!th.isAlive()) {
					th.start();
				}
				agent.upOrDown = false;
			}
		});
	}

	protected class Agent implements Runnable {

		/**
		 * @param upOrDown
		 * 			boolean that says which button was pressed.
		 * @param stop
		 * 			boolean that says if counter has to stop
		 * @param counter
		 * 			int for counter. Will be displayed in the label.
		 */
		private volatile boolean upOrDown;
		private volatile boolean stop;
		private int counter;
		@Override
		public void run() {
			
			while (!this.stop) {
				if (this.upOrDown == true)  {
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								ConcurrentGUI.this.label.setText(Integer.toString(Agent.this.counter));
							}
						});
						this.counter++;
						Thread.sleep(100);
					} catch (InvocationTargetException | InterruptedException ex) {
						System.out.println("an error of " + ex.getClass() + " type occurred.");
						ex.printStackTrace();
					}
				}
				else {
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								ConcurrentGUI.this.label.setText(Integer.toString(Agent.this.counter));
							}
						});
						this.counter--;
						Thread.sleep(100);
					} catch (InvocationTargetException | InterruptedException ex) {
						System.out.println("an error of " + ex.getClass() + " type occurred.");
						ex.printStackTrace();
					}
				}
			}
			
		}
		
		public void setUpOrDown(final boolean choice) {
			this.upOrDown = choice;
		}
		
		public void stopCounting() {
			this.stop = true;
		}
	}
}
