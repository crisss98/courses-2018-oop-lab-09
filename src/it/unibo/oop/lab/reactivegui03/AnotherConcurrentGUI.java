package it.unibo.oop.lab.reactivegui03;

import it.unibo.oop.lab.reactivegui02.ConcurrentGUI;
/**
 * 
 * GUI for the exercise.
 *
 */
public class AnotherConcurrentGUI extends ConcurrentGUI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int N_SEC = 10;
	/**
	 * constructor for the GUI.
	 */
	public AnotherConcurrentGUI() {
		super();
		final Agent2 agent2 = new Agent2();
		new Thread(agent2).start();
	}
	
	private class Agent2 implements Runnable {

		public void run() {
				for (int i = 0; i < N_SEC; i++) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						System.out.println("an error occurred.");
						e.printStackTrace();
					}
				}
				agent.stopCounting();
				up.setEnabled(false);
				down.setEnabled(false);
				stop.setEnabled(false);
				label.setText("this program is Apple made. Incurred in programmed obsolescence");
		}
	}
}
