package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadedSumMatrix implements SumMatrix {

	private final int threadNumber;
	
	public MultiThreadedSumMatrix(final int n) {
		// TODO Auto-generated constructor stub
		this.threadNumber = n;
		//final List<Worker> workerList = new LinkedList<>();
	}
	
	private static class Worker extends Thread {
		private final int startPos;
		private final int endPos;
		private final double[][] matrix;
		private double res = 0;
		final int matrixSize;
		
		public Worker(final int startPos, final int endPos, final double[][] matrix) {
			// TODO Auto-generated constructor stub
			super();
			this.startPos = startPos;
			this.endPos = endPos;
			this.matrix = matrix;
			matrixSize = (matrix[0].length * matrix.length);
		}

		public double getResult() {
			// TODO Auto-generated method stub
			return this.res;
		}
		
		public void run() {
			//sum the element in res
			for (int i = startPos; i < matrixSize && i < startPos + endPos; i++) {
				this.res += matrix[i / matrix[0].length][i % matrix[0].length];
			}
		}
	}
	@Override
	public double sum(final double[][] matrix) {
	    final int matrixSize = (matrix[0].length * matrix.length);
		final int size = matrixSize % threadNumber + matrixSize / threadNumber;
		// TODO Auto-generated method stub
		final List<Worker> worker = new ArrayList<>(threadNumber);
		for (int start = 0; start < matrixSize; start += size) {
			worker.add(new Worker (start, size, matrix));
		}
		for (final Worker w: worker) {
			w.start();
		}
		double res = 0;
		for (final Worker w: worker) {
			try {
				w.join();
				res += w.getResult();
			} catch (InterruptedException ex) {
				System.out.println("a InterruptedException occurred");
				throw new IllegalStateException(ex);
			}
		}
		return res;
	}

}
